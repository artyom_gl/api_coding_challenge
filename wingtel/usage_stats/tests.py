from datetime import datetime, date, timedelta
from decimal import Decimal

from django.core.management import call_command
from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIRequestFactory

from wingtel.att_subscriptions.models import ATTSubscription
from wingtel.usage_stats.factories import PlanFactory, ATTSubscriptionFactory, SprintSubscriptionFactory, \
    VoiceUsageRecordFactory, DataUsageRecordFactory
from wingtel.usage_stats.models import DataStats, VoiceStats


class SetUp(TestCase):

    def data_usage_batch_create(self, size, entry_price=0.0, sub=False, usage_date=date.today()):

        sub = {'att_subscription_id': sub} if isinstance(sub, ATTSubscription) else {'sprint_subscription_id': sub}
        kwargs = {
            **sub,
            'kilobytes_used': 0,
            'price': entry_price,
            'usage_date': usage_date,
            'size': size
        }
        DataUsageRecordFactory.create_batch(**kwargs)

    def voice_usage_batch_create(self, size, entry_price=0.0, sub=False, usage_date=date.today()):

        sub = {'att_subscription_id': sub} if isinstance(sub, ATTSubscription) else {'sprint_subscription_id': sub}
        kwargs = {
            **sub,
            'seconds_used': 0,
            'price': entry_price,
            'usage_date': usage_date,
            'size': size
        }
        VoiceUsageRecordFactory.create_batch(**kwargs)

    def setUp(self):

        self.plan = PlanFactory(name='Cheap Plan', price=50)
        self.att_sub = ATTSubscriptionFactory(plan=self.plan)
        self.sprint_sub = SprintSubscriptionFactory(plan=self.plan)
        self.request_factory = APIRequestFactory()

        self.back_7_days = date.today() - timedelta(days=7)
        self.back_7_days_str = datetime.strftime(date.today() - timedelta(days=7), '%Y-%m-%d')
        self.today = date.today()
        self.today_str = datetime.strftime(date.today(), '%Y-%m-%d')
        self.yesterday = date.today() - timedelta(days=1)
        self.yesterday_str = datetime.strftime(date.today() - timedelta(days=1), '%Y-%m-%d')


class StatsTest(SetUp):

    def test_empty_data_usage_stats(self):

        response = self.client.get(
            reverse('type_usage_stats', args=[self.yesterday_str, self.today_str, 'data']),
            format='json',
        )
        self.assertEqual(response.data, [])

    def test_empty_voice_usage_stats(self):

        response = self.client.get(
            reverse('type_usage_stats', args=[self.yesterday_str, self.today_str, 'voice']),
            format='json',
        )
        self.assertEqual(response.data, [])

    def test_present_data_usage_stats_att(self):

        self.data_usage_batch_create(size=10, entry_price=Decimal(5), sub=self.att_sub, usage_date=self.yesterday)

        call_command('generate_usage_stats', '--forced')

        response = self.client.get(
            reverse('type_usage_stats', args=[self.yesterday_str, self.today_str, 'data']),
            format='json',
        )
        expected_response = {
            'att_subscription_id': self.att_sub.id,
            'usage_frequency': 10,
            'total_usage_cost': Decimal('50.00')
        }
        self.assertDictEqual(response.data[0], expected_response)

    def test_present_voice_usage_stats_att(self):

        self.voice_usage_batch_create(size=10, entry_price=Decimal(5), sub=self.att_sub, usage_date=self.yesterday)

        call_command('generate_usage_stats', '--forced')

        response = self.client.get(
            reverse('type_usage_stats', args=[self.yesterday_str, self.today_str, 'voice']),
            format='json',
        )
        expected_response = {
            'att_subscription_id': self.att_sub.id,
            'usage_frequency': 10,
            'total_usage_cost': Decimal('50.00')
        }
        self.assertDictEqual(response.data[0], expected_response)

    def test_present_data_usage_stats_sprint(self):

        self.data_usage_batch_create(size=10, entry_price=Decimal(5), sub=self.sprint_sub, usage_date=self.yesterday)

        call_command('generate_usage_stats', '--forced')

        response = self.client.get(
            reverse('type_usage_stats', args=[self.yesterday_str, self.today_str, 'data']),
            format='json',
        )
        expected_response = {
            'sprint_subscription_id': self.sprint_sub.id,
            'usage_frequency': 10,
            'total_usage_cost': Decimal('50.00')
        }
        self.assertDictEqual(response.data[0], expected_response)

    def test_present_voice_usage_stats_sprint(self):

        self.voice_usage_batch_create(size=10, entry_price=Decimal(5), sub=self.sprint_sub, usage_date=self.yesterday)

        call_command('generate_usage_stats', '--forced')

        response = self.client.get(
            reverse('type_usage_stats', args=[self.yesterday_str, self.today_str, 'voice']),
            format='json',
        )
        expected_response = {
            'sprint_subscription_id': self.sprint_sub.id,
            'usage_frequency': 10,
            'total_usage_cost': Decimal('50.00')
        }
        self.assertDictEqual(response.data[0], expected_response)

    def test_present_data_usage_stats_sprint_and_att(self):

        self.data_usage_batch_create(size=10, entry_price=Decimal(5), sub=self.sprint_sub, usage_date=self.yesterday)
        self.data_usage_batch_create(size=10, entry_price=Decimal(5), sub=self.att_sub, usage_date=self.yesterday)

        call_command('generate_usage_stats', '--forced')

        response = self.client.get(
            reverse('type_usage_stats', args=[self.yesterday_str, self.today_str, 'data']),
            format='json',
        )
        expected_response = [
            {
                'sprint_subscription_id': self.sprint_sub.id,
                'usage_frequency': 10,
                'total_usage_cost': Decimal('50.00')
            }, {
                'att_subscription_id': self.att_sub.id,
                'usage_frequency': 10,
                'total_usage_cost': Decimal('50.00')
            }
        ]
        self.assertEqual(response.data, expected_response)

    def test_present_voice_usage_stats_sprint_and_att(self):

        self.voice_usage_batch_create(size=10, entry_price=Decimal(5), sub=self.sprint_sub, usage_date=self.yesterday)
        self.voice_usage_batch_create(size=10, entry_price=Decimal(5), sub=self.att_sub, usage_date=self.yesterday)

        call_command('generate_usage_stats', '--forced')

        response = self.client.get(
            reverse('type_usage_stats', args=[self.yesterday_str, self.today_str, 'voice']),
            format='json',
        )
        expected_response = [
            {
                'sprint_subscription_id': self.sprint_sub.id,
                'usage_frequency': 10,
                'total_usage_cost': Decimal('50.00')
            }, {
                'att_subscription_id': self.att_sub.id,
                'usage_frequency': 10,
                'total_usage_cost': Decimal('50.00')
            }
        ]
        self.assertEqual(response.data, expected_response)

    def test_correct_range_for_data_usage_stats_sprint_and_att(self):

        self.data_usage_batch_create(size=10, entry_price=Decimal(5), sub=self.sprint_sub, usage_date=self.back_7_days)
        self.data_usage_batch_create(size=10, entry_price=Decimal(5), sub=self.att_sub, usage_date=self.back_7_days)

        self.data_usage_batch_create(size=10, entry_price=Decimal(5), sub=self.sprint_sub, usage_date=self.yesterday)
        self.data_usage_batch_create(size=10, entry_price=Decimal(5), sub=self.att_sub, usage_date=self.yesterday)

        call_command('generate_usage_stats', '--forced')

        response = self.client.get(
            reverse('type_usage_stats', args=[self.yesterday_str, self.today_str, 'data']),
            format='json',
        )
        expected_response = [
            {
                'sprint_subscription_id': self.sprint_sub.id,
                'usage_frequency': 10,
                'total_usage_cost': Decimal('50.00')
            }, {
                'att_subscription_id': self.att_sub.id,
                'usage_frequency': 10,
                'total_usage_cost': Decimal('50.00')
            }
        ]
        self.assertEqual(response.data, expected_response)

    def test_correct_range_for_voice_usage_stats_sprint_and_att(self):

        self.voice_usage_batch_create(size=10, entry_price=Decimal(5), sub=self.sprint_sub, usage_date=self.back_7_days)
        self.voice_usage_batch_create(size=10, entry_price=Decimal(5), sub=self.att_sub, usage_date=self.back_7_days)

        self.voice_usage_batch_create(size=10, entry_price=Decimal(5), sub=self.sprint_sub, usage_date=self.yesterday)
        self.voice_usage_batch_create(size=10, entry_price=Decimal(5), sub=self.att_sub, usage_date=self.yesterday)

        call_command('generate_usage_stats', '--forced')

        response = self.client.get(
            reverse('type_usage_stats', args=[self.yesterday_str, self.today_str, 'voice']),
            format='json',
        )
        expected_response = [
            {
                'sprint_subscription_id': self.sprint_sub.id,
                'usage_frequency': 10,
                'total_usage_cost': Decimal('50.00')
            }, {
                'att_subscription_id': self.att_sub.id,
                'usage_frequency': 10,
                'total_usage_cost': Decimal('50.00')
            }
        ]
        self.assertEqual(response.data, expected_response)


class ExceedingLimitTest(SetUp):

    def test_limit_exceeding_data_usage_stats_sprint_and_att(self):

        self.data_usage_batch_create(size=10, entry_price=Decimal(5), sub=self.sprint_sub, usage_date=self.yesterday)
        self.data_usage_batch_create(size=10, entry_price=Decimal(5), sub=self.att_sub, usage_date=self.today)

        call_command('generate_usage_stats', '--forced')

        response = self.client.get(
            reverse('price_exceeded_stats', args=[41]),
            format='json',
        )

        expected_response = [
            {
                'sprint_subscription_id': self.sprint_sub.id,
                'data_exceeded_usage': Decimal('9.00')
            }, {
                'att_subscription_id': self.att_sub.id,
                'data_exceeded_usage': Decimal('9.00')
            }
        ]

        self.assertEqual(response.data, expected_response)

    def test_limit_exceeding_voice_usage_stats_sprint_and_att(self):

        self.voice_usage_batch_create(size=10, entry_price=Decimal(5), sub=self.sprint_sub, usage_date=self.yesterday)
        self.voice_usage_batch_create(size=10, entry_price=Decimal(5), sub=self.att_sub, usage_date=self.today)

        call_command('generate_usage_stats', '--forced')

        response = self.client.get(
            reverse('price_exceeded_stats', args=[41]),
            format='json',
        )

        expected_response = [
            {
                'sprint_subscription_id': self.sprint_sub.id,
                'voice_exceeded_usage': Decimal('9.00')
            }, {
                'att_subscription_id': self.att_sub.id,
                'voice_exceeded_usage': Decimal('9.00')
            }
        ]

        self.assertEqual(response.data, expected_response)

    def test_limit_not_exceeding_data_usage_stats_sprint_and_att(self):

        self.data_usage_batch_create(size=10, entry_price=Decimal(5), sub=self.sprint_sub, usage_date=self.yesterday)
        self.data_usage_batch_create(size=10, entry_price=Decimal(5), sub=self.att_sub, usage_date=self.today)

        call_command('generate_usage_stats', '--forced')

        response = self.client.get(
            reverse('price_exceeded_stats', args=[51]),
            format='json',
        )

        self.assertEqual(response.data, [])

    def test_limit_not_exceeding_voice_usage_stats_sprint_and_att(self):

        self.data_usage_batch_create(size=10, entry_price=Decimal(5), sub=self.sprint_sub, usage_date=self.yesterday)
        self.data_usage_batch_create(size=10, entry_price=Decimal(5), sub=self.att_sub, usage_date=self.today)

        call_command('generate_usage_stats', '--forced')

        response = self.client.get(
            reverse('price_exceeded_stats', args=[51]),
            format='json',
        )

        self.assertEqual(response.data, [])


class ManageCommandTest(SetUp):

    def test_generate_usage_stats_incremental_mode_data_usage_sprint_and_att(self):

        self.data_usage_batch_create(size=10, entry_price=Decimal(5), sub=self.sprint_sub, usage_date=self.back_7_days)
        self.data_usage_batch_create(size=10, entry_price=Decimal(5), sub=self.att_sub, usage_date=self.back_7_days)

        call_command('generate_usage_stats')

        self.assertEqual(2, DataStats.objects.count())

        self.data_usage_batch_create(size=1, entry_price=Decimal(5), sub=self.sprint_sub, usage_date=self.yesterday)
        self.data_usage_batch_create(size=1, entry_price=Decimal(5), sub=self.att_sub, usage_date=self.yesterday)

        call_command('generate_usage_stats')

        self.assertEqual(4, DataStats.objects.count())

    def test_generate_usage_stats_incremental_mode_voice_usage_sprint_and_att(self):

        self.voice_usage_batch_create(size=10, entry_price=Decimal(5), sub=self.sprint_sub, usage_date=self.back_7_days)
        self.voice_usage_batch_create(size=10, entry_price=Decimal(5), sub=self.att_sub, usage_date=self.back_7_days)

        call_command('generate_usage_stats')

        self.assertEqual(2, VoiceStats.objects.count())

        self.voice_usage_batch_create(size=1, entry_price=Decimal(5), sub=self.sprint_sub, usage_date=self.yesterday)
        self.voice_usage_batch_create(size=1, entry_price=Decimal(5), sub=self.att_sub, usage_date=self.yesterday)

        call_command('generate_usage_stats')

        self.assertEqual(4, VoiceStats.objects.count())
