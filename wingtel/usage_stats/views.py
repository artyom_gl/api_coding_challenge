from datetime import date
from decimal import Decimal
from typing import List

from django.db.models import Sum
from django.db.models.functions import Coalesce
from rest_framework import generics
from rest_framework.renderers import JSONRenderer, BrowsableAPIRenderer
from rest_framework.response import Response

from wingtel.att_subscriptions.models import ATTSubscription
from wingtel.sprint_subscriptions.models import SprintSubscription
from wingtel.usage_stats.serializers import ExceededPriceSerializer, MetricsByUsageTypeSerializer


class UsageStatsExceededPriceView(generics.ListAPIView):
    """
    Endpoint which allows to get subscriptions usage which had exceeded certain price limit
    """
    renderer_classes = [BrowsableAPIRenderer, JSONRenderer]
    queryset = ''

    def _aggregate_usage_cost(self, subscription, usage_type) -> Decimal:

        previous_usage = None
        todays_usage = None

        if usage_type == 'data':
            previous_usage = subscription.voicestats_set
            todays_usage = subscription.voiceusagerecord_set
        elif usage_type == 'voice':
            previous_usage = subscription.datastats_set
            todays_usage = subscription.datausagerecord_set

        previous_usage_cost = previous_usage.aggregate(
            total_price=Coalesce(Sum('total_price'), 0)
        )
        todays_usage_cost = todays_usage.filter(usage_date__date=date.today()).aggregate(
            total_price=Coalesce(Sum('price'), 0)
        )

        return previous_usage_cost['total_price'] + todays_usage_cost['total_price']

    def _collect_exceeded_usage(self, limit, subscription_type) -> List[dict]:
        not_null_stats = []

        for sub in subscription_type.objects.all().iterator(chunk_size=100):
            voice_usage = self._aggregate_usage_cost(sub, 'data')
            data_usage = self._aggregate_usage_cost(sub, 'voice')

            if any([voice_usage, data_usage]):
                results = {'att_subscription_id': sub.id} if isinstance(sub, ATTSubscription) else {'sprint_subscription_id': sub.id}
                data_present = False
                if voice_usage > limit:
                    results['voice_exceeded_usage'] = voice_usage - limit
                    data_present = True
                if data_usage > limit:
                    results['data_exceeded_usage'] = data_usage - limit
                    data_present = True

                if data_present:
                    not_null_stats.append(results)

        return not_null_stats

    def get_stats_of_exceeded_usage(self, limit) -> List[dict]:
        sprint_stats = self._collect_exceeded_usage(limit, SprintSubscription)
        att_stats = self._collect_exceeded_usage(limit, ATTSubscription)

        return sprint_stats + att_stats

    def get(self, request, **kwargs):
        serializer = ExceededPriceSerializer(data=kwargs)
        if serializer.is_valid(raise_exception=True):
            limit = serializer.validated_data['limit']
            return Response(self.get_stats_of_exceeded_usage(limit))


class UsageStatsByTypeView(generics.ListAPIView):
    """
    Endpoint for retrieving usage cost in dates range
    """
    renderer_classes = [BrowsableAPIRenderer, JSONRenderer]
    queryset = ''

    @staticmethod
    def _collect_stats(from_date, to_date, usage_type, subscription_type) -> List[dict]:
        stats_type = None
        not_null_stats = []
        for sub in subscription_type.objects.all().iterator(chunk_size=100):
            if usage_type == 'voice':
                stats_type = sub.voicestats_set
            elif usage_type == 'data':
                stats_type = sub.datastats_set

            stats = stats_type.filter(report_date__range=[from_date, to_date]).\
                aggregate(
                    usage_frequency=Sum('total_usage'),
                    total_usage_cost=Sum('total_price')
                )

            if any(stats.values()):
                sub_type = 'att' if isinstance(sub, ATTSubscription) else 'sprint'
                not_null_stats.append({f'{sub_type}_subscription_id': sub.id, **stats})

        return not_null_stats

    def get_stats(self, from_date, to_date, usage_type) -> List[dict]:
        sprint_stats = self._collect_stats(from_date, to_date, usage_type, SprintSubscription)
        att_stats = self._collect_stats(from_date, to_date, usage_type, ATTSubscription)

        return sprint_stats + att_stats

    def get(self, request, **kwargs):
        serializer = MetricsByUsageTypeSerializer(data=kwargs)
        if serializer.is_valid(raise_exception=True):
            from_date, to_date, usage_type = serializer.validated_data.values()
            return Response(self.get_stats(from_date, to_date, usage_type))
