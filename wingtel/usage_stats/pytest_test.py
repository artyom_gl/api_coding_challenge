from datetime import datetime, date, timedelta
from decimal import Decimal

from django.core.management import call_command
from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIRequestFactory

from wingtel.att_subscriptions.models import ATTSubscription
from wingtel.usage_stats.factories import PlanFactory, ATTSubscriptionFactory, SprintSubscriptionFactory, \
    VoiceUsageRecordFactory, DataUsageRecordFactory
from wingtel.usage_stats.models import DataStats, VoiceStats

import pytest



def test_numbers_3_4():
    print("test 3*4")
    assert 3*4 == 12