from rest_framework import serializers


class MetricsByUsageTypeSerializer(serializers.Serializer):
    from_date = serializers.DateField()
    to_date = serializers.DateField()
    usage_type = serializers.ChoiceField(choices=['voice', 'data'])

    def validate(self, data):
        """
        Check that date's in right odrer
        """
        if data['from_date'] > data['to_date']:
            raise serializers.ValidationError("from_date should precede to_date")
        return data
