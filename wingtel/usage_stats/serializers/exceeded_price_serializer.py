from rest_framework import serializers


class ExceededPriceSerializer(serializers.Serializer):
    limit = serializers.DecimalField(max_digits=7, decimal_places=2, min_value=0.01)
