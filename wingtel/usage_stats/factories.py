import factory
from django.contrib.auth.models import User

from wingtel.att_subscriptions.models import ATTSubscription
from wingtel.plans.models import Plan
from wingtel.sprint_subscriptions.models import SprintSubscription
from wingtel.usage.models import DataUsageRecord, VoiceUsageRecord


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    username = factory.Sequence(lambda n: "Agent %03d" % n)
    password = factory.PostGenerationMethodCall('set_password', 'swordfish')


class PlanFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Plan


class ATTSubscriptionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ATTSubscription

    user = factory.SubFactory(UserFactory)


class SprintSubscriptionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = SprintSubscription

    class Params:
        language = factory.SubFactory(UserFactory)

    user = factory.SubFactory(UserFactory)


class DataUsageRecordFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = DataUsageRecord


class VoiceUsageRecordFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = VoiceUsageRecord
