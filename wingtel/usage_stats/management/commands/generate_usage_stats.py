from datetime import date, timedelta

from django.core.management.base import BaseCommand
from django.db.models import Count, Sum
from django.db.models.functions import TruncDate
from django.test import override_settings

from wingtel.att_subscriptions.models import ATTSubscription
from wingtel.sprint_subscriptions.models import SprintSubscription
from wingtel.usage_stats.models import DataStats, VoiceStats


class Command(BaseCommand):
    help = 'Populate DailySubscriptionUsageStats table with new entries'

    def add_arguments(self, parser):
        parser.add_argument('-f', '--forced', action='store_true')

    def handle(self, *args, **options):
        if options['forced']:
            self.stdout.write(self.style.SUCCESS('forced stats generation'))
            DataStats.objects.all().delete()
            VoiceStats.objects.all().delete()

            for sub in SprintSubscription.objects.all().iterator(chunk_size=10):
                self.generate_stats(sub, 'voice', forced=True)
                self.generate_stats(sub, 'data', forced=True)
            for sub in ATTSubscription.objects.all().iterator(chunk_size=10):
                self.generate_stats(sub, 'voice', forced=True)
                self.generate_stats(sub, 'data', forced=True)
        else:
            self.stdout.write(self.style.SUCCESS('incremental stats generation'))

            for sub in SprintSubscription.objects.all().iterator(chunk_size=10):
                self.generate_stats(sub, 'voice')
                self.generate_stats(sub, 'data')
            for sub in ATTSubscription.objects.all().iterator(chunk_size=10):
                self.generate_stats(sub, 'voice')
                self.generate_stats(sub, 'data')
            # generate stats from last recorded day up until now

    def _get_trunced_dates(self, sub, usage_type, forced):
        """Define from which date we want to generate reports, and what's type, implement --forced flag"""

        if usage_type == 'data':
            related_set = sub.datausagerecord_set
            if not forced:
                reports = sub.datastats_set.exists()
                if reports:  # Because initially they are like from forced
                    above_last_report_date = sub.datastats_set.latest('report_date').report_date + timedelta(days=1)
                    related_set = sub.datausagerecord_set.filter(usage_date__gte=above_last_report_date)
            trunced_dates = related_set.exclude(usage_date__date=date.today()).annotate(
                date=TruncDate('usage_date'))
            usage_data = {'total_kilobytes_used': Sum('kilobytes_used')}
            return trunced_dates, usage_data

        elif usage_type == 'voice':
            related_set = sub.voiceusagerecord_set
            if not forced:
                reports = sub.voicestats_set.exists()
                if reports:  # Because initially they are like from forced
                    above_last_report_date = sub.voicestats_set.latest('report_date').report_date + timedelta(days=1)
                    related_set = sub.voiceusagerecord_set.filter(usage_date__gte=above_last_report_date)
            trunced_dates = related_set.exclude(usage_date__date=date.today()).annotate(
                date=TruncDate('usage_date'))
            usage_data = {'total_seconds_used': Sum('seconds_used')}
            return trunced_dates, usage_data

    def generate_stats(self, subscription, usage_type, forced=False) -> None:
        """Forcefully recreate stats models up to yesterday, present day intentionally omitted"""
        sub = subscription

        trunced_dates, usage_data = self._get_trunced_dates(subscription, usage_type, forced)

        for day in trunced_dates.values('date').distinct():
            totals = {
                **trunced_dates.filter(usage_date__range=[day['date'], day['date'] + timedelta(days=1)]) \
                    .aggregate(
                    total_usage=Count('id'),
                    total_price=Sum('price'),
                    **usage_data,
                )
            }
            sub_fk = {'att_subscription_id': sub} if isinstance(sub, ATTSubscription) else {'sprint_subscription_id': sub}
            timestamp = {'report_date': day['date']}

            if usage_type == 'data':
                DataStats.objects.create(**sub_fk, **timestamp, **totals)
            elif usage_type == 'voice':
                VoiceStats.objects.create(**sub_fk, **timestamp, **totals)
