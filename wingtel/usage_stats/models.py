from django.db import models

from wingtel.att_subscriptions.models import ATTSubscription
from wingtel.sprint_subscriptions.models import SprintSubscription


class DailySubscriptionUsageStats(models.Model):
    att_subscription_id = models.ForeignKey(ATTSubscription, null=True, on_delete=models.PROTECT)
    sprint_subscription_id = models.ForeignKey(SprintSubscription, null=True, on_delete=models.PROTECT)

    total_price = models.DecimalField(decimal_places=2, max_digits=7, default=0)
    total_usage = models.IntegerField(default=0)
    report_date = models.DateField(null=False)

    class Meta:
        abstract = True


class DataStats(DailySubscriptionUsageStats):
    """Subscription data usage stats from one day"""
    total_kilobytes_used = models.IntegerField(default=0)


class VoiceStats(DailySubscriptionUsageStats):
    """Subscription voice usage stats from one day"""
    total_seconds_used = models.IntegerField(default=0)

    # Optimistic quality of 3g calling ~ 1.5 kylobytes(12 kbit) == second
