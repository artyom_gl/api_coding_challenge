from django.apps import AppConfig


class UsageStatsConfig(AppConfig):
    name = 'wingtel.usage_stats'
    label = 'usage_stats'
