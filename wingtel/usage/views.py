from rest_framework import viewsets

from wingtel.usage.models import DataUsageRecord, VoiceUsageRecord
from wingtel.usage.serializers import DataUsageSerializer, VoiceUsageSerializer


class DataUsageViewSet(viewsets.ModelViewSet):
    queryset = DataUsageRecord.objects.all()
    serializer_class = DataUsageSerializer


class VoiceUsageViewSet(viewsets.ModelViewSet):
    queryset = VoiceUsageRecord.objects.all()
    serializer_class = VoiceUsageSerializer
