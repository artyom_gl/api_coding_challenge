

#DataUsageRecord/VoiceUsageRecord - price
у подписки берётся ONE_KILOBYTE_PRICE если это DataUsageRecord(либо ONE_SECOND_PRICE если это VoiceUsageRecord)
и соответственно умножается ONE_KILOBYTE_PRICE на kilobytes_used(либо ONE_SECOND_PRICE на seconds_used)
так мы получаем цену одного DataUsageRecord/VoiceUsageRecord


### Aggregated Usage Models
Для каждого дня в который есть созданные DataUsageRecord/VoiceUsageRecord
создаётся соответствующий DailySubscriptionUsageStats своего типа
который вычисляет дневную стоимость оказанных услуг и хранит у себя


### API - Subscriptions Exceeding Usage Price Limit
берём все связанные с подпиской DataStats/VoiceStats и смотрим лимит цены в запросе
затем обьединяем DataUsageRecord/VoiceUsageRecord в один список,
и суммируем последовательно пока не достигаем числа лимита.
Если лимит достигнут а usage ещё есть, создаём отдельные счётчики превышения для каждого типа usage
и возвращаем соответственно превышение по каждому типу usage
{[
    {'subscription_id': 1, 'exceeded_data_usage_cost':5.52},
    {'subscription_id': 2, 'exceeded_data_usage_cost':5.52, 'exceeded_voice_usage_cost': 9.00}
]}


### API - Usage Metrics By Subscription and Usage Type
Берём все DataStats или VoiceStats в промежутке между from date, to date 
для каждой подписки суммируем price из DataStats или VoiceStats соответственно
и суммируем количество использований подписки в этом промежутке

{[
    {'subscription_id': 1, 'total_usage_cost':155.4, 'usage_frequency': 62},
    {'subscription_id': 2, 'total_usage_cost':24.5, 'usage_frequency': 5},
]}